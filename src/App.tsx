import React from 'react';
import logo from './logo.svg';
import './App.css';
import { TextFeild } from './TextFeild';
import Greet from './Greet';
import Person from './Person';
import PersonList, { List, personName } from './PersonList';
import Message from './Message';
import UseStateComponent from './UseStateComponent';
import UseEffectComponent from './UseEffectComponent';
import TypeAliases from './TypeAliases';


function App() {
 
  return (
    <div className="App">
      <TextFeild name='Anup Shrestha' id={10} />
      <TextFeild name=' Shrestha' id={110} />

      <Greet name='Anup' messageCount={10} isLoggedIn={true} />
      <Person  name={personName} />
      <PersonList names={List}/>
      <Message message='Loading'/>
      <UseStateComponent/>
      <UseEffectComponent/>
      <TypeAliases title='nawwaraj'/>
    </div>
  );
}

export default App;
