interface IGreetProps {
  name: String;
  isLoggedIn: boolean;
  messageCount: Number;
}
const Greet = (props: IGreetProps) => {
  // let greetingMessage;
//   if (props.isLoggedIn) {
//     greetingMessage = `${props.name}! You have ${props.messageCount} unread messages. `
// }
// else{
//   greetingMessage ="Welcome Guest"
// }

  return (
    <div>
      {
            props.isLoggedIn ? `${props.name}! You have ${props.messageCount} unread messages. ` : "Welcome Guest"
        }
        {/* {greetingMessage} */}
    </div>
  )
}

export default Greet