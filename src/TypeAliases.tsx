// export const TypeAliases = ({title}:{title:string}) => {
//     return (
//       <div>{title}</div>
//     )
//   }

                // OR
type Props = {
    title: string
  }

  
  const TypeAliases = ({title}:Props) => {
    return (
      <div>{title}</div>
    )
  }

  export default TypeAliases

    //   OR

    // type Props = {
    //     title: string
    //   }

//   const TypeAliases:React.FC<Props> = ({title}) => {
//     return (
//       <div>{title}</div>
//     )
//   }
// export default TypeAliases
  
