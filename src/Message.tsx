interface IMessageProps{
    message: "Loading" | "Success" | "Error"
}

const Message = (props:IMessageProps) => {
let text;
{
    props.message=== 'Loading'?text ='Loading...' :
    props.message=== 'Success'?text ='Data Fetched Successfully...' :
    props.message=== 'Error'? text ='Error Fetching Data...': text =" not valid input" ;
}

  return (
    <div>
        <h2>{text}</h2>
    </div>
  )
}

export default Message